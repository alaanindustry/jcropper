<?php

namespace Model;

use Mediator;

Class Model extends \Connection{

	protected static $table = "Users";

	public function query($columns,$options=[])
	{
		$result = array();
		$conditions = array();
		$where = array();
		$condition = array();

		for ($i=0; $i < count($options); $i++) { 
			if (strtoupper($options[$i])!="AND" && strtoupper($options[$i])!="OR") {
				$where[] = $options[$i];
			}else{
				$condition[] = strtoupper($options[$i]);
				$conditions[] = $where;
				$where = array();
			}
			if ($i==(count($options)-1)) {
				$conditions[] = $where;
			}
		}

		$stick_where = "";
		$counter_condition = 0;

		for ($i=0; $i < count($conditions); $i++) { 
			if (fmod($i, 1)==0) {
				if ($i!=0) {
					$stick_where = $stick_where.' '.$condition[$counter_condition].' ';
					$counter_condition++;
				}else{
					$stick_where = $stick_where.' Where ';
				}
				$stick_where = $stick_where.$conditions[$i][0].'=:'.$conditions[$i][0];
			}
		}
		$query 		= 'SELECT '.$columns.' FROM '.self::$table.$stick_where;
		$connection = self::getConnection();
		$statement 	= $connection->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
		for ($i=0; $i < count($conditions); $i++) { 
			$statement->bindParam(':'.$conditions[$i][0], $s = $conditions[$i][1]);
		}
		$statement->execute();
		$result = $statement->fetchAll(\PDO::FETCH_ASSOC);
		return $result;	
	}
	
	public function create($values=[])
	{	
		$columns = "";
		$values_param = "";
		$query = "SHOW columns from ".self::$table;
		$connection = self::getConnection();
		$statement 	= $connection->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
		$statement->execute();
		$result = $statement->fetchAll();

		foreach ($result as $key => $value) {
			$key = $value[0];
			$key_value  = isset($values[$value[0]])?$values[$value[0]]:'';
			if ($key_value!='') {
				$columns = $columns.$key.',';
			}
		}

		foreach ($result as $key => $value) {
			$key = $value[0];
			$key_value  = isset($values[$value[0]])?$values[$value[0]]:'';
			if ($key_value!='') {
				$values_param = $values_param.':'.$key.',';
			}
		}

		// for ($i=1; $i < count($result); $i++) { 
		// 	$columns = $columns.$result[$i][0].',';
		// }
		$columns = rtrim($columns,',');
		// for ($i=1; $i < count($result); $i++) { 
		// 	$values_param = $values_param.':'.$result[$i][0].',';
		// }
		$values_param = rtrim($values_param,',');

		$query = "INSERT into ".self::$table." (".$columns.") Values(".$values_param.")";

		$statement1 = $connection->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
		
		foreach ($result as $key => $value) {
			$key = $value[0];
			$key_value  = isset($values[$value[0]])?$values[$value[0]]:'';

			if ($key_value!='') {
				$statement1->bindValue(':'.$key, $key_value);
			}
		}
		$statement1->execute();
		return $connection->lastInsertId();
	}

	public function set($sets = [],$id)
	{
		$set = "";
		$splitsets = array();
		foreach ($sets as $key => $value) {
			$set = $set.$key.'=:'.$key.',';
		}
		$set = rtrim($set,',');
		$connection = self::getConnection();
		$query = "UPDATE ".self::$table." set ". $set." WHERE id='".$id."';";
		$statement = $connection->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));

		foreach ($sets as $key => $value) {
			$statement->bindValue(':'.$key, $s = $value);
		}
		$statement->execute();
	}

	public function delete($id)
	{	
		$connection = self::getConnection();
		$query = "DELETE FROM ".self::$table." WHERE id='".$id."';";
		$statement = $connection->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
		$statement->execute();
	}
}