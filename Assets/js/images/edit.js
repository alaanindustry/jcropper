new Vue({
	el:'#image-edit',
	ready:function() {
		var $image = $('#image');
		$image.cropper({
		 	viewMode:1,
	        autoCropArea: 1,
		 })
	},
	methods:{
		updateImage:function(argument) {
			var $form = document.querySelector('#form-edit-image');
			var formdata = new FormData($form);
			var cropcanvas = $('#image').cropper('getCroppedCanvas');
			var croppng = cropcanvas.toDataURL("image/png");
			formdata.append('image',croppng);
			$.ajax({
				url: '/image/updateimage',
				type: 'POST',
				dataType: 'json',
				data: formdata,
				contentType: false,
				cache: false, 
				processData:false, 
			})
			.done(function(response) {
        		$('#form-group-title').removeClass('has-error');
        		$('#help-title').text('');
				if (response.path){
					config.toast("success","Successfully updated Image");
        			console.log("sucess");
        		}else{
        			$.each(response, function(index, val) {
        				$('#form-group-'+index).addClass('has-error');
        				$('#help-'+index).text(val);
        			});
        		}
				console.log("success");
			});
		},
		cropImage:function() {
			$('#image').cropper('getCroppedCanvas').toBlob(function(blob) {
				var newImg = $('#image');
				url = URL.createObjectURL(blob);
				$('#image').cropper('replace', url);
				newImg.src = URL.createObjectURL(blob);
				newImg.onload = function() {
				    URL.revokeObjectURL(this.src);
				};
			})
		},
		done:function (argument) {
			window.location.href = "/";
		}
	}
});