// define
var MyComponent = Vue.extend({
	props: ['imagePath','title','description','imageId','rowIndex'],
	template: `
	  	<div class="thumbnail" :id="imageId">
		    <img :src="'/Assets/img/'+imagePath" alt="">
		      <div class="caption">
		        <h3>{{title}}</h3>
		        <p>{{description}}</p>
		        <p>
		        	<button type="button" class="btn btn-primary" @click="editImage(imageId)">
				      	Edit Image
					</button>
					<button type="button" class="btn btn-danger" @click="deleteImage(imageId,rowIndex)">
				      	Delete
					</button>
				</p>
			    </div>
	    </div>
	`,
	methods: {
		editImage:function(id) {
			window.location.href = "/images/edit/"+id;
		},
		deleteImage:function(id,row) {
			var vue= this;
			if (window.confirm("Are you sure you want to delete this?")==1) {
				$.ajax({
					url: '/image/deleteimage/'+id,
					type: 'GET',
					dataType: 'json'
				})
				.done(function(response) {
					config.toast("success","Successfully Deleted");
					vue.$dispatch('image-delete', row)
				});
			}
		}
	}
})
// register
Vue.component('image-component', MyComponent)
// create a root instance
new Vue({
	el: '#images',
	data:{
		images: [],
		image: {
			"id": 0,
			"title": '',
			"desciption": '',
			"path": ''
		},
		imageEdit : new FormData()
	},
	events:{
		'image-delete':function(row) {
			this.images.splice(row, 1);
		}
	},
	ready:function(){
		this.getImages();
		var $image = $('#image');
	    var cropBoxData;
	    var canvasData;
      	$('#modal').on('shown.bs.modal', function () {
	        $image.cropper({
	        	viewMode:3,
	          	autoCropArea: 0.5,
	          	ready: function () {
	          		console.log("ready");
		            $image.cropper('setCanvasData', canvasData);
		            $image.cropper('setCropBoxData', cropBoxData);
		        }
	        });
	    }).on('hidden.bs.modal', function () {
	        cropBoxData = $image.cropper('getCropBoxData');
	        canvasData = $image.cropper('getCanvasData');
	        $image.cropper('destroy');
	    });
	},
	methods:{
		getImages:function(){
			var vue = this;
			$.ajax({
				url: '/images/getimages',
				type: 'GET',
				dataType: 'json',
			})
			.done(function(images) {
				vue.images = images;
			});
		},
		storeImage:function() {
			var vue = this;
			var form_upload = document.querySelector('#form-image');
			$.ajax({
        		url: "images/storeimage", 
				type: "POST",
				dataType: "json",
				data: new FormData(form_upload), 
				contentType: false,       
				cache: false,
				processData:false,
        	})
        	.done(function(response) {
        		$('#form-group-image').removeClass('has-error');
        		$('#form-group-title').removeClass('has-error');
        		$('#help-title').text('');
        		$('#help-image').text('');
        		if (response.id){
        			config.toast("success","Successfully uploaded Image");
        			console.log("sucess");
        			$('[name="title"]').val('');
        			$('[name="description"]').val('');
        			vue.images.push(response);
        		}else{
        			$.each(response, function(index, val) {
        				$('#form-group-'+index).addClass('has-error');
        				$('#help-'+index).text(val);
        			});
        		}
        	});
		},
		cropImage:function() {
			var $image = $('#crop-picture');
			$image.cropper('getCroppedCanvas').toBlob(function (blob) {
				console.log(blob);
				var formData = new FormData();
				formData.append('croppedImage', blob);
				formData.append('token','sdf');
				$.ajax('/image/uploadblobimage', {
				    method: "POST",
				    data: formData,
				    processData: false,
				    contentType: false,
				    success: function (response) {
				      console.log('Upload success:'+response);
				    },
				    error: function () {
				      console.log('Upload error');
				    }
				});
			});
		},
		updateImage:function(argument) {
			alert("sdf");
		}
	}	
});