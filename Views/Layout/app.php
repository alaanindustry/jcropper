<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Image Cropper</title>
    <link href="<?php echo Config\App::url('/Assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo Config\App::url('Assets/css/jumbotron-narrow.css'); ?>" rel="stylesheet">
    <link href="<?php echo Config\App::url('/Assets/css/toaster.css'); ?>" rel="stylesheet" type="text/css" >
    <link href="<?php echo Config\App::url('/Assets/css/cropper.css'); ?>" rel="stylesheet" type="text/css" >
    <script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/jquery-3.1.0.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/masonry.js'); ?>"></script>
  </head>

  <body>
    <div class="container">
      <div class="header clearfix">
        <h3 class="text-muted">Image Cropper</h3>
      </div>
      <!-- </div> -->
  </body>
</html>