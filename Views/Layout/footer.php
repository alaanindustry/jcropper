
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/vue.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/vue-resource.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/vue-validator.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/toaster.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/cropper.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/config.js'); ?>"></script>