<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container" id="images">
		<div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
			        <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			            <h4 class="modal-title">Edit Picture</h4>
			        </div>
			        <div class="modal-body">
			        <!-- /Jcrop/Assets/img/PGradient-Background-For-Desktop.jpg -->
			        	<div v-if="!image.title">
			        		Please wait...
			        	</div>
			        	<form v-if="image.title" id="form-edit-image" enctype="multipart/form-data" @submit.prevent>
							<div class="form-group">
							    <label>Image Preview</label><br>
							    <div class="thumbnail">
							      <img id="image" :src="'/Jcrop/assets/img/' + image.path" alt="">
							    </div><button type="" class="btn btn-primary" @click="cropImage()">Crop Image</button>
							</div>
							<div class="form-group">
							    <label>Title</label>
							    <input name="title" v-model="image.title" class="form-control" placeholder="Enter image Title">
							    <span id="help-title" class="help-block"></span>
							</div>
							<div class="form-group">
							    <label>Description</label>
							    <textarea name="description" v-model="image.description" class="form-control" rows="3" placeholder="Enter image description"></textarea>
							</div>
						</form>
			        </div>
			        <div class="modal-footer">
			            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </div>
		    </div>
	    </div>
		<div class="row">
			<div class="col-xs-6">
				<form id="form-image" enctype="multipart/form-data" @submit.prevent>
				  <div class="form-group">
				    <label>Image Preview</label>
				    <div class="thumbnail" style="min-height: 200px">
				      <img src="" alt="" id="image-preview">
				    </div>
				  </div>
				  <div class="form-group" id="form-group-title">
				    <label>Title</label>
				    <input name="title" class="form-control" placeholder="Enter image Title">
				    <span id="help-title" class="help-block"></span>
				  </div>
				  <div class="form-group">
				    <label>Description</label>
				    <textarea name="description" class="form-control" rows="3" placeholder="Enter image description"></textarea>
				  </div>
				  <div class="form-group" id="form-group-image">
				    <label >Select Image</label>
				    <input name="image" type="file" class="form-control" id="crop-image">
				    <p class="help-block" id="help-image"></p>
				  </div>
				  <button type="button" class="btn btn-primary" @click="storeImage()">Upload Image</button>
				</form>
			</div>
		</div><hr>
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-sm-6 col-md-4" v-for="(index,image) in images" id="column-images"> 
					    <image-component :image-id="image.id" :row-index="index" :image-path="image.path" :title="image.title" :description="image.description"></image-component>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/images/index.js'); ?>"></script>
  	<script type="text/javascript">
    (function() {
        var URL = window.URL || window.webkitURL;
        var input = document.querySelector('#crop-image');
        var preview = document.querySelector('#image-preview');
        input.addEventListener('change', function () {
            preview.src = URL.createObjectURL(this.files[0]);
        });
        preview.addEventListener('load', function () {
            URL.revokeObjectURL(this.src);
        });
    })();
  </script>
</body>
</html> 