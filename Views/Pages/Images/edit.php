<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container" id="image-edit">
		<form @submit.prevent id="form-edit-image" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo($ai->image['id']) ?>">
			<div class="form-group">
			    <label for="exampleInputEmail1">Image Crop</label>
			    <div class="thumbnail">
			      	<img id="image" name="image" src="<?php echo "/Assets/img/".$ai->image['path']; ?>" alt="...">
			    </div>
			    <button type="button" class="btn btn-primary" @click="cropImage()">Crop Image</button>
			</div>
			<div class="form-group" id="form-group-title">
			    <label for="exampleInputEmail1">Title</label>
			    <input name="title" id="image-title" type="text" value="<?php echo $ai->image['title']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Image Title">
			    <span id="help-title" class="help-block"></span>
			</div>
			<div class="form-group">
			    <label for="exampleInputPassword1">Description</label>
			    <textarea rows="5" name="description" id="image-description" type="text"  class="form-control" id="exampleInputPassword1"><?php echo $ai->image['description']; ?></textarea>
			</div>
			<button type="button" class="btn btn-primary" @click="updateImage()">Update Image</button>
			<button type="button" class="btn btn-success" @click="done()">Done</button>
		</form>
		<hr>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/images/edit.js'); ?>"></script>
</body>
</html> 