<?php

namespace Route;

use Route\Route;

Class App{

	function __construct() {

		/*
		|-------------------------------------------------------------------------------
		| Image Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('','ImageController#index');
		Route::get('images/getimages','ImageController#getImages');
		Route::get('images/edit/{id}','ImageController#edit');
		Route::post('images/storeimage','ImageController#store');
		Route::post('image/updateimage','ImageController#update');
		Route::get('image/deleteimage/{id}','ImageController#delete');
	}
}

