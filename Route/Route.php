<?php 


namespace Route;

Class Route{

	public static $get = array();
	public static $post = array();

	public static function get($value,$controller)
	{
		$route = [$value,$controller];
		array_push(self::$get, $route);
	}

	public static function post($value,$controller)
	{
		$route = [$value,$controller];
		array_push(self::$post, $route);
	}

	public function redirect($value='')
	{
		# code...
	}
}