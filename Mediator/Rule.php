<?php

namespace Mediator;

use Model;

Class Rule{

	private $password = '';

	public function name($name)
	{

		if (preg_match("/[a-zA-Z{2,}-]/", $name)) {
		}else{
			return 'Please enter the proper name';
		}

	}

	public function email($email)
	{
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

			Model\User::setSettings();
			$user = Model\User::query("*",["username",$email]);

			if (count($user)==0) {

			}
		    
		} else {
		    return 'Please enter the right email address';
		}
	}

	public function email_exist($email)
	{
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

			$user = Model\User::query("*",["username",$email]);

			if (count($user)==0) {

			}else{
				return 'Email already exists.';
			}
		    
		} else {
		    return 'Please enter the right email address';
		}
	}

	public function password($password)
	{
		$this->password = $password;
		if (preg_match("/./", $password)) {
		}else{
			return 'Please enter the proper password';
		}
	}

	public function confirm($confirm_password)
	{
		if ($this->password===$confirm_password) {
		}else{
			return 'Passwords did not matched';
		}
	}

	public function double($value)
	{
		if (!preg_match('/^[0-9]+(\\.[0-9]+)?$/', $value)) {
			return "Please enter the right amount";
		}
	}

	public function number($value)
	{
		if (!preg_match('/^[0-9]+$/', $value)) {
			return "Please enter the right number";
		}
	}

	public function phone($phone)
	{
		if (!preg_match('/^(09|\+639)\d{9}$/', $phone)) {
			return "Invalid Philippine Number";
		}
	}

	public function image($image)
	{
		$acceptable = array(
				        'image/jpeg',
				        'image/jpg',
				        'image/gif',
				        'image/png'
				    );
		if((!in_array($image, $acceptable)) || (empty($image))) {
	        return 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
	    }
	}
}