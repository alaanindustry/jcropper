<?php


namespace Responses;

Class Old{


	function __construct($request) {

		if ($request!=null) {
			foreach ($request as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	public function get($param)
	{
		if (property_exists($this, $param)) {
			echo $this->$param;
		}
		echo "";
	}

	public function has($param)
	{
		if (property_exists($this, $param)) {
			return true;
		}
		return false;
	}

}