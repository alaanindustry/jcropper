<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class ImageController{

	function __construct()
	{
	}
	public function index()
	{
		return Views\App::view('Pages.Images.index');
	}
	public function validate(Requests\Request $request)
	{
		$validator = new Mediator\Validator(['name'],$request);
		if (!$validator->isValid()) {
			return $validator->getErrors();
		}
	}
	public function store(Requests\Request $request)
	{
		$validateValues = [
    		'title' => $request->title,
    		'image' => $request->files->image['type']
    	];$validator = new Mediator\Validator(['name','image'],$validateValues);

    	if ($validator->isValid()) {
    		$save_path = '/Assets/img/products/'.basename($request->files->image['name']);
    		$app_path = '/Assets/img/';
    		$target_dir = $_SERVER['DOCUMENT_ROOT'].$app_path;
    		$base_name = basename($request->files->image['name']);
			$target_file = $target_dir . $base_name;
			move_uploaded_file($request->files->image['tmp_name'], $target_file);

			Model\Image::setSettings();
            $image = Model\Image::create([
                'title'  => $request->title,
                'description'  => $request->description,
                'path' => $base_name
            ]);

            return ["id"=>$image,"path"=>$base_name,"title"=>$request->title,"description"=>$request->description];
    	}else{
    		return $validator->getErrors();
    	}
	}

	public function edit($id)
	{
		Model\Image::setSettings();
		$image = \Raw::query('select * from images where id = "'.$id.'" limit 1');
		return Views\App::view('Pages.Images.edit',['image'=>$image[0]]);
	}
	public function getImages()
	{
		Model\Image::setSettings();
		$images = Model\Image::query('*');
		return $images;
	}
	public function update(Requests\Request $request)
	{
		$validateValues = [
    		'title' => $request->title,
    	];$validator = new Mediator\Validator(['name'],$validateValues);
    	if ($validator->isValid()) {
    		$path = $this->generateRandomString();
			$filename = 'Assets/img/'.$path.'.png';
			$img = $request->image;
			$img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			file_put_contents($filename, $data);
			Model\Image::setSettings();
			Model\Image::set([
				'title' => $request->title,
				'description' => $request->description,
				'path' => $path.".png",
			],$request->id);
			return ["title"=>$request->title,"description"=>$request->description,'path'=>$path.".png"];
    	}else{
    		return $validator->getErrors();
    	}
	}
	public function delete($id)
	{
		Model\Image::setSettings();
        Model\Image::delete($id);
        return "success";
	}
	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}