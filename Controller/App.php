<?php

namespace Controller;

use Route;
use Responses;
use Requests;
use Views;

Class App{

	private $controller = 'HomeController';

	private $method = 'index';

	private $url_method = 'get';


	function __construct() {

		$uri = "$_SERVER[REQUEST_URI]";

		$uri = ltrim($uri, '/');
		$chunk_uri = explode("/", $uri);
		// array_splice($chunk_uri, 0, 1);
		$stick_uri = implode("/", $chunk_uri);

		new Route\App();

		if ($this->checkURI($chunk_uri)) {

			$chunk_controller = explode("#", $this->controller);
			$controller = 'Controller\\'.$chunk_controller[0];
			$this->controller = $controller;
			$this->method = $chunk_controller[1];
			array_splice($chunk_uri, 0, 2);
			
			$params = $chunk_uri;
			$request = $_REQUEST;
			unset($_REQUEST);
			$files = new Requests\File($_FILES);
			unset($_FILES);
			if ($this->url_method=="get") {
				$result = call_user_func_array([new $this->controller,$this->method], $params);
				if ($result!=null) {
					echo json_encode($result, JSON_PRETTY_PRINT);
				}
			}else{
				$request = new Requests\Request($request);
				$request->files = $files;
				$result = call_user_func_array([new $this->controller,$this->method], [$request]);
				if ($result!=null) {
					echo json_encode($result, JSON_PRETTY_PRINT);
				}
			}
		}else{
			Views\App::view('Auth.error');
		}

	}

	public function checkURI($uri)
	{
		$registered_routes = Route\Route::$get;
		$number_of_registered = 0;
		if ($_REQUEST==null) {
			for ($i=0; $i < count($registered_routes); $i++) {
				$chunk_registered_uri = explode('/', $registered_routes[$i][0]);
				$uri_count = count($uri);
				$registered_routes_count = count($chunk_registered_uri);
				if ($uri_count==$registered_routes_count) {
					if ($uri_count==1) {
						if ($uri[0]==$chunk_registered_uri[0]) {
							$this->controller = $registered_routes[$i][1];
							$number_of_registered++;
						}
					}else{
						$base_uri = $uri[0].$uri[1];
						$base_uri_registered = $chunk_registered_uri[0].$chunk_registered_uri[1];
						if ($base_uri==$base_uri_registered) {
							$this->controller = $registered_routes[$i][1];
							$this->method = $uri[1];
							$number_of_registered++;
						}
					}
				}
			}
			return ($number_of_registered!=0);
		}else{
			$registered_routes = Route\Route::$post;
			for ($i=0; $i < count($registered_routes); $i++) {

				$chunk_registered_uri = explode('/', $registered_routes[$i][0]);
				$uri_count = count($uri);
				$registered_routes_count = count($chunk_registered_uri);
				if ($uri_count==$registered_routes_count) {
					if ($uri_count==1) {
						if ($uri[0]==$chunk_registered_uri[0]) {
							$this->controller = $registered_routes[$i][1];
							$number_of_registered++;
							$this->url_method = 'post';
						}
					}else{
						$base_uri = $uri[0].$uri[1];
						$base_uri_registered = $chunk_registered_uri[0].$chunk_registered_uri[1];
						if ($base_uri==$base_uri_registered) {
							$this->controller = $registered_routes[$i][1];
							$this->method = $uri[1];
							$number_of_registered++;
							$this->url_method = 'post';
						}
					}
				}
			}
			return ($number_of_registered!=0);
		}
	}
}