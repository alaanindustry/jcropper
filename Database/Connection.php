<?php 

Class Connection extends Config{

	public function getConnection()
	{
		$con = "mysql:host=" . self::$host . "; dbname=" . self::$database;
		$connection = new PDO($con, self::$username, self::$password);
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $connection;
	}
}